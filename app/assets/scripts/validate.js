function showError(container, errorMessage) {
  container.className = 'form-field form-field_error';
  var msgElem = document.createElement('span');
  msgElem.className = "form-field__error-message";
  msgElem.innerHTML = errorMessage;
  container.appendChild(msgElem);
}

function resetError(container) {
  container.className = '';
  if (container.lastChild.className == "form-field__error-message") {
    container.removeChild(container.lastChild);
  }
}

function validate(form) {
  var elems = form.elements;
  
  console.log(form);
  
  resetError(elems.name.parentNode);
  if (!elems.name.value) {
    showError(elems.name.parentNode, ' Укажите имя');
  }
  
  resetError(elems.email.parentNode);
  if (!elems.email.value) {
    showError(elems.email.parentNode, ' Укажите электронную почту');
  }
  
  resetError(elems.phone.parentNode);
  if (!elems.phone.value) {
    showError(elems.phone.parentNode, ' Укажите номер телефона');
  }
}
